# Version: 0.0.1

FROM ubuntu:14.04
MAINTAINER Salvador Rodriguez <salvador.rodriguez@utah.edu>

# Install packages
ENV REFRESHED_AT 2015-01-14
RUN apt-get update && \
    apt-get install -yq --no-install-recommends wget unzip pwgen ca-certificates && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV TOMCAT_MAJOR_VERSION 8
ENV TOMCAT_MINOR_VERSION 8.0.11
ENV CATALINA_HOME /tomcat

# Install Java
RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main"\
    > /etc/apt/sources.list.d/webupd8team-java.list \
    && echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main"\
    >> /etc/apt/sources.list.d/webupd8team-java.list \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886 \
    && apt-get update -y \
    && echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections \
    && apt-get install -y --no-install-recommends oracle-java8-installer=8u25+8u6arm-1~webupd8~1 \
    && apt-get autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set java home	
RUN echo JAVA_HOME=/usr/lib/jvm/java-8-oracle >> ~/.bashrc
RUN echo PATH=$JAVA_HOME/bin:$PATH:$HOME/bin >> ~/.bashrc
RUN echo export PATH JAVA_HOME >> ~/.bashrc

# Install TOMCAT
ENV TOMCAT_MAJOR_VERSION 8
ENV TOMCAT_MINOR_VERSION 8.0.11
ENV CATALINA_HOME /tomcat

RUN wget -q https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz && \
    wget -qO- https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz.md5 | md5sum -c - && \
    tar zxf apache-tomcat-*.tar.gz && \
    rm apache-tomcat-*.tar.gz && \
    mv apache-tomcat* tomcat

ADD create_tomcat_admin_user.sh /create_tomcat_admin_user.sh

# Install OpenCDS
RUN mkdir /.opencds/
ADD opencds.properties /root/.opencds/opencds.properties
ADD opencds-apelon.properties /root/.opencds/opencds-apelon.properties
ADD opencds-guvnor.properties /root/.opencds/opencds-guvnor.properties
ADD guvnor-users.properties /root/.opencds/guvnor-users.properties

ADD opencds-knowledge-repository-data.zip /opencds-knowledge-repository-data.zip
RUN unzip opencds-knowledge-repository-data.zip -d /root/
RUN rm opencds-knowledge-repository-data.zip

ADD opencds-decision-support-service.war /tomcat/webapps/opencds-decision-support-service.war
ADD opencds-apelon.war /tomcat/webapps/opencds-apelon.war

ADD run.sh /run.sh
RUN chmod +x /*.sh

EXPOSE 8080
CMD ["/run.sh"]

