In order to build this image, you must include the following files in the same folder were dockerfile is located:
- opencds-decision-support-service.war
- 

Build image :
sudo docker build -t="opencds/opencds:ubuntu" .

Create container :
sudo docker run --name opencds -i -t opencds/opencds:ubuntu

Opencds service will be available on x.x.x.x:8080/opencds-decision-support-service. You can 

Find out the ipaddress of the container:
sudo docker inspect apelon | grep IPAddress

Information about opencds service is available on http://www.opencds.org/


